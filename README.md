### Installation

`helloer` expects Ruby 2.3.0 and Bundler to be present.
For Ubuntu, [ruby-ng](https://launchpad.net/~brightbox/+archive/ubuntu/ruby-ng) is the simpliest way to get those.

The setup itself is as easy as `bundle install`.

### Running

To launch server on port 3000, run `bundle exec ruby server.rb -e prod -p 3000`

To specify a folder to serve static files, you can pass an additional `-f <path>` parameter. If no path specified, files are server from current folder. Subdirectories are not supported when serving, for securoty reasons (otherwise it wou;d be too easy to request something like `localhost:3000/../../../etc/passwd`, and I'm too lazy to do proper filtering).
