require 'goliath'
require 'grape'

FILE_ROOT = File.dirname(__FILE__)

class API < Grape::API
  get '*' do
    content_type 'application/octet-stream'
    File.binread File.join(env.options[:folder], File.basename(request.path))
  end
end

class Application < Goliath::API

  def options_parser(opts, options)
    options[:folder] = File.dirname(__FILE__)
    opts.on('-f', '--folder FOLDER', "Folder to serve files from") { |val| options[:folder] = val }
  end

  def response(env)
    API.call(env)
  end
end
